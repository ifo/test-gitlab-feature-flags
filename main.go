package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/Unleash/unleash-client-go/v3"
)

func init() {
	unleash.Initialize(
		unleash.WithListener(&unleash.DebugListener{}),
		unleash.WithInstanceId(os.Getenv("INSTANCE_ID")),
		unleash.WithAppName("dev"),
		unleash.WithUrl(os.Getenv("API_URL")),
	)
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		if unleash.IsEnabled("response_change") {
			fmt.Fprint(w, "enabled")
		} else {
			fmt.Fprintf(w, "disabled")
		}
	})
	log.Fatalln(http.ListenAndServe(":3000", nil))
}
