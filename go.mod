module gitlab.com/ifo/test-gitlab-feature-flags

go 1.17

require (
	github.com/Unleash/unleash-client-go/v3 v3.2.4
	github.com/twmb/murmur3 v1.1.5 // indirect
)
